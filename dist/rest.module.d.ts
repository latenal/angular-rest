import { ModuleWithProviders } from '@angular/core';
import { RestServiceConfig } from './services';
export declare class RestClientModule {
    static forRoot(config?: RestServiceConfig): ModuleWithProviders;
}
