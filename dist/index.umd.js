/**
 * angular-rest - Angular Rest Client
 * @version v1.0.8
 * @author Artziel Narvaiza
 * @link undefined
 * @license MIT
 */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("@angular/core"), require("@angular/common/http"), require("rxjs/operators"), require("@angular/common"), require("rxjs/Subject"), require("rxjs/add/operator/do"), require("rxjs/add/operator/map"));
	else if(typeof define === 'function' && define.amd)
		define(["@angular/core", "@angular/common/http", "rxjs/operators", "@angular/common", "rxjs/Subject", "rxjs/add/operator/do", "rxjs/add/operator/map"], factory);
	else if(typeof exports === 'object')
		exports["angular-rest"] = factory(require("@angular/core"), require("@angular/common/http"), require("rxjs/operators"), require("@angular/common"), require("rxjs/Subject"), require("rxjs/add/operator/do"), require("rxjs/add/operator/map"));
	else
		root["angular-rest"] = factory(root["ng"]["core"], root["ng"]["commonHttp"], root["Rx"], root["ng"]["common"], root["Rx"], root["Rx"], root["Rx"]);
})(typeof self !== 'undefined' ? self : this, function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_5__, __WEBPACK_EXTERNAL_MODULE_8__, __WEBPACK_EXTERNAL_MODULE_15__, __WEBPACK_EXTERNAL_MODULE_17__, __WEBPACK_EXTERNAL_MODULE_18__, __WEBPACK_EXTERNAL_MODULE_19__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return COOKIE_OPTIONS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CookieOptionsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__angular_common__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils__ = __webpack_require__(3);



var COOKIE_OPTIONS = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["InjectionToken"]('COOKIE_OPTIONS');
/** @private */
var CookieOptionsProvider = (function () {
    function CookieOptionsProvider(options, _injector) {
        if (options === void 0) { options = {}; }
        this._injector = _injector;
        this.defaultOptions = {
            path: this._injector.get(__WEBPACK_IMPORTED_MODULE_1__angular_common__["APP_BASE_HREF"], '/'),
            domain: null,
            expires: null,
            secure: false,
            httpOnly: false
        };
        this._options = Object(__WEBPACK_IMPORTED_MODULE_2__utils__["d" /* mergeOptions */])(this.defaultOptions, options);
    }
    Object.defineProperty(CookieOptionsProvider.prototype, "options", {
        get: function () {
            return this._options;
        },
        enumerable: true,
        configurable: true
    });
    CookieOptionsProvider.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    /** @nocollapse */
    CookieOptionsProvider.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"], args: [COOKIE_OPTIONS,] },] },
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injector"], },
    ]; };
    return CookieOptionsProvider;
}());



/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CookieService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__cookie_options_provider__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__utils__ = __webpack_require__(3);



var CookieService = (function () {
    function CookieService(_optionsProvider) {
        this._optionsProvider = _optionsProvider;
        this.options = this._optionsProvider.options;
    }
    Object.defineProperty(CookieService.prototype, "cookieString", {
        get: function () {
            return document.cookie || '';
        },
        set: function (val) {
            document.cookie = val;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @name CookieService#get
     *
     * @description
     * Returns the value of given cookie key.
     *
     * @param {string} key Id to use for lookup.
     * @returns {string} Raw cookie value.
     */
    /**
       * @name CookieService#get
       *
       * @description
       * Returns the value of given cookie key.
       *
       * @param {string} key Id to use for lookup.
       * @returns {string} Raw cookie value.
       */
    CookieService.prototype.get = /**
       * @name CookieService#get
       *
       * @description
       * Returns the value of given cookie key.
       *
       * @param {string} key Id to use for lookup.
       * @returns {string} Raw cookie value.
       */
    function (key) {
        return this._cookieReader()[key];
    };
    /**
     * @name CookieService#getObject
     *
     * @description
     * Returns the deserialized value of given cookie key.
     *
     * @param {string} key Id to use for lookup.
     * @returns {Object} Deserialized cookie value.
     */
    /**
       * @name CookieService#getObject
       *
       * @description
       * Returns the deserialized value of given cookie key.
       *
       * @param {string} key Id to use for lookup.
       * @returns {Object} Deserialized cookie value.
       */
    CookieService.prototype.getObject = /**
       * @name CookieService#getObject
       *
       * @description
       * Returns the deserialized value of given cookie key.
       *
       * @param {string} key Id to use for lookup.
       * @returns {Object} Deserialized cookie value.
       */
    function (key) {
        var value = this.get(key);
        return value ? Object(__WEBPACK_IMPORTED_MODULE_2__utils__["f" /* safeJsonParse */])(value) : value;
    };
    /**
     * @name CookieService#getAll
     *
     * @description
     * Returns a key value object with all the cookies.
     *
     * @returns {Object} All cookies
     */
    /**
       * @name CookieService#getAll
       *
       * @description
       * Returns a key value object with all the cookies.
       *
       * @returns {Object} All cookies
       */
    CookieService.prototype.getAll = /**
       * @name CookieService#getAll
       *
       * @description
       * Returns a key value object with all the cookies.
       *
       * @returns {Object} All cookies
       */
    function () {
        return this._cookieReader();
    };
    /**
     * @name CookieService#put
     *
     * @description
     * Sets a value for given cookie key.
     *
     * @param {string} key Id for the `value`.
     * @param {string} value Raw value to be stored.
     * @param {CookieOptions} options (Optional) Options object.
     */
    /**
       * @name CookieService#put
       *
       * @description
       * Sets a value for given cookie key.
       *
       * @param {string} key Id for the `value`.
       * @param {string} value Raw value to be stored.
       * @param {CookieOptions} options (Optional) Options object.
       */
    CookieService.prototype.put = /**
       * @name CookieService#put
       *
       * @description
       * Sets a value for given cookie key.
       *
       * @param {string} key Id for the `value`.
       * @param {string} value Raw value to be stored.
       * @param {CookieOptions} options (Optional) Options object.
       */
    function (key, value, options) {
        this._cookieWriter()(key, value, options);
    };
    /**
     * @name CookieService#putObject
     *
     * @description
     * Serializes and sets a value for given cookie key.
     *
     * @param {string} key Id for the `value`.
     * @param {Object} value Value to be stored.
     * @param {CookieOptions} options (Optional) Options object.
     */
    /**
       * @name CookieService#putObject
       *
       * @description
       * Serializes and sets a value for given cookie key.
       *
       * @param {string} key Id for the `value`.
       * @param {Object} value Value to be stored.
       * @param {CookieOptions} options (Optional) Options object.
       */
    CookieService.prototype.putObject = /**
       * @name CookieService#putObject
       *
       * @description
       * Serializes and sets a value for given cookie key.
       *
       * @param {string} key Id for the `value`.
       * @param {Object} value Value to be stored.
       * @param {CookieOptions} options (Optional) Options object.
       */
    function (key, value, options) {
        this.put(key, JSON.stringify(value), options);
    };
    /**
     * @name CookieService#remove
     *
     * @description
     * Remove given cookie.
     *
     * @param {string} key Id of the key-value pair to delete.
     * @param {CookieOptions} options (Optional) Options object.
     */
    /**
       * @name CookieService#remove
       *
       * @description
       * Remove given cookie.
       *
       * @param {string} key Id of the key-value pair to delete.
       * @param {CookieOptions} options (Optional) Options object.
       */
    CookieService.prototype.remove = /**
       * @name CookieService#remove
       *
       * @description
       * Remove given cookie.
       *
       * @param {string} key Id of the key-value pair to delete.
       * @param {CookieOptions} options (Optional) Options object.
       */
    function (key, options) {
        this._cookieWriter()(key, undefined, options);
    };
    /**
     * @name CookieService#removeAll
     *
     * @description
     * Remove all cookies.
     */
    /**
       * @name CookieService#removeAll
       *
       * @description
       * Remove all cookies.
       */
    CookieService.prototype.removeAll = /**
       * @name CookieService#removeAll
       *
       * @description
       * Remove all cookies.
       */
    function (options) {
        var _this = this;
        var cookies = this.getAll();
        Object.keys(cookies).forEach(function (key) {
            _this.remove(key, options);
        });
    };
    CookieService.prototype._cookieReader = function () {
        var lastCookies = {};
        var lastCookieString = '';
        var cookieArray, cookie, i, index, name;
        var currentCookieString = this.cookieString;
        if (currentCookieString !== lastCookieString) {
            lastCookieString = currentCookieString;
            cookieArray = lastCookieString.split('; ');
            lastCookies = {};
            for (i = 0; i < cookieArray.length; i++) {
                cookie = cookieArray[i];
                index = cookie.indexOf('=');
                if (index > 0) {
                    // ignore nameless cookies
                    name = Object(__WEBPACK_IMPORTED_MODULE_2__utils__["e" /* safeDecodeURIComponent */])(cookie.substring(0, index));
                    // the first value that is seen for a cookie is the most
                    // specific one.  values for the same cookie name that
                    // follow are for less specific paths.
                    if (Object(__WEBPACK_IMPORTED_MODULE_2__utils__["a" /* isBlank */])(lastCookies[name])) {
                        lastCookies[name] = Object(__WEBPACK_IMPORTED_MODULE_2__utils__["e" /* safeDecodeURIComponent */])(cookie.substring(index + 1));
                    }
                }
            }
        }
        return lastCookies;
    };
    CookieService.prototype._cookieWriter = function () {
        var that = this;
        return function (name, value, options) {
            that.cookieString = that._buildCookieString(name, value, options);
        };
    };
    CookieService.prototype._buildCookieString = function (name, value, options) {
        var opts = Object(__WEBPACK_IMPORTED_MODULE_2__utils__["d" /* mergeOptions */])(this.options, options);
        var expires = opts.expires;
        if (Object(__WEBPACK_IMPORTED_MODULE_2__utils__["a" /* isBlank */])(value)) {
            expires = 'Thu, 01 Jan 1970 00:00:00 GMT';
            value = '';
        }
        if (Object(__WEBPACK_IMPORTED_MODULE_2__utils__["c" /* isString */])(expires)) {
            expires = new Date(expires);
        }
        var cookieValue = opts.storeUnencoded ? value : encodeURIComponent(value);
        var str = encodeURIComponent(name) + '=' + cookieValue;
        str += opts.path ? ';path=' + opts.path : '';
        str += opts.domain ? ';domain=' + opts.domain : '';
        str += expires ? ';expires=' + expires.toUTCString() : '';
        str += opts.secure ? ';secure' : '';
        str += opts.httpOnly ? '; HttpOnly' : '';
        // per http://www.ietf.org/rfc/rfc2109.txt browser must allow at minimum:
        // - 300 cookies
        // - 20 cookies per unique domain
        // - 4096 bytes per cookie
        var cookieLength = str.length + 1;
        if (cookieLength > 4096) {
            console.log("Cookie '" + name + "' possibly not set or overflowed because it was too \n      large (" + cookieLength + " > 4096 bytes)!");
        }
        return str;
    };
    CookieService.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    /** @nocollapse */
    CookieService.ctorParameters = function () { return [
        { type: __WEBPACK_IMPORTED_MODULE_1__cookie_options_provider__["b" /* CookieOptionsProvider */], },
    ]; };
    return CookieService;
}());



/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = isBlank;
/* harmony export (immutable) */ __webpack_exports__["b"] = isPresent;
/* harmony export (immutable) */ __webpack_exports__["c"] = isString;
/* harmony export (immutable) */ __webpack_exports__["d"] = mergeOptions;
/* harmony export (immutable) */ __webpack_exports__["e"] = safeDecodeURIComponent;
/* harmony export (immutable) */ __webpack_exports__["f"] = safeJsonParse;
function isBlank(obj) {
    return obj === undefined || obj === null;
}
function isPresent(obj) {
    return obj !== undefined && obj !== null;
}
function isString(obj) {
    return typeof obj === 'string';
}
function mergeOptions(oldOptions, newOptions) {
    if (!newOptions) {
        return oldOptions;
    }
    return {
        path: isPresent(newOptions.path) ? newOptions.path : oldOptions.path,
        domain: isPresent(newOptions.domain) ? newOptions.domain : oldOptions.domain,
        expires: isPresent(newOptions.expires) ? newOptions.expires : oldOptions.expires,
        secure: isPresent(newOptions.secure) ? newOptions.secure : oldOptions.secure,
        storeUnencoded: isPresent(newOptions.storeUnencoded) ? newOptions.storeUnencoded : oldOptions.storeUnencoded,
    };
}
function safeDecodeURIComponent(str) {
    try {
        return decodeURIComponent(str);
    }
    catch (e) {
        return str;
    }
}
function safeJsonParse(str) {
    try {
        return JSON.parse(str);
    }
    catch (e) {
        return str;
    }
}


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var rest_service_1 = __webpack_require__(11);
exports.RestClientService = rest_service_1.RestClientService;
var rest_config_1 = __webpack_require__(9);
exports.RestServiceConfig = rest_config_1.RestServiceConfig;


/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_5__;

/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CookieModule", function() { return CookieModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__src_cookie_options_provider__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__src_cookie_service__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__src_cookie_factory__ = __webpack_require__(7);
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "CookieService", function() { return __WEBPACK_IMPORTED_MODULE_2__src_cookie_service__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__src_cookie_backend_service__ = __webpack_require__(16);
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "CookieBackendService", function() { return __WEBPACK_IMPORTED_MODULE_4__src_cookie_backend_service__["a"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "COOKIE_OPTIONS", function() { return __WEBPACK_IMPORTED_MODULE_1__src_cookie_options_provider__["a"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "CookieOptionsProvider", function() { return __WEBPACK_IMPORTED_MODULE_1__src_cookie_options_provider__["b"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "cookieServiceFactory", function() { return __WEBPACK_IMPORTED_MODULE_3__src_cookie_factory__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__src_utils__ = __webpack_require__(3);
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "isBlank", function() { return __WEBPACK_IMPORTED_MODULE_5__src_utils__["a"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "isPresent", function() { return __WEBPACK_IMPORTED_MODULE_5__src_utils__["b"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "isString", function() { return __WEBPACK_IMPORTED_MODULE_5__src_utils__["c"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "mergeOptions", function() { return __WEBPACK_IMPORTED_MODULE_5__src_utils__["d"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "safeDecodeURIComponent", function() { return __WEBPACK_IMPORTED_MODULE_5__src_utils__["e"]; });
/* harmony namespace reexport (by provided) */ __webpack_require__.d(__webpack_exports__, "safeJsonParse", function() { return __WEBPACK_IMPORTED_MODULE_5__src_utils__["f"]; });









var CookieModule = (function () {
    function CookieModule() {
    }
    /**
     * Use this method in your root module to provide the CookieService
     * @param {CookieOptions} options
     * @returns {ModuleWithProviders}
     */
    /**
       * Use this method in your root module to provide the CookieService
       * @param {CookieOptions} options
       * @returns {ModuleWithProviders}
       */
    CookieModule.forRoot = /**
       * Use this method in your root module to provide the CookieService
       * @param {CookieOptions} options
       * @returns {ModuleWithProviders}
       */
    function (options) {
        if (options === void 0) { options = {}; }
        return {
            ngModule: CookieModule,
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_1__src_cookie_options_provider__["a" /* COOKIE_OPTIONS */], useValue: options },
                { provide: __WEBPACK_IMPORTED_MODULE_2__src_cookie_service__["a" /* CookieService */], useFactory: __WEBPACK_IMPORTED_MODULE_3__src_cookie_factory__["a" /* cookieServiceFactory */], deps: [__WEBPACK_IMPORTED_MODULE_1__src_cookie_options_provider__["b" /* CookieOptionsProvider */]] }
            ]
        };
    };
    /**
     * Use this method in your other (non root) modules to import the directive/pipe
     * @param {CookieOptions} options
     * @returns {ModuleWithProviders}
     */
    /**
       * Use this method in your other (non root) modules to import the directive/pipe
       * @param {CookieOptions} options
       * @returns {ModuleWithProviders}
       */
    CookieModule.forChild = /**
       * Use this method in your other (non root) modules to import the directive/pipe
       * @param {CookieOptions} options
       * @returns {ModuleWithProviders}
       */
    function (options) {
        if (options === void 0) { options = {}; }
        return {
            ngModule: CookieModule,
            providers: [
                { provide: __WEBPACK_IMPORTED_MODULE_1__src_cookie_options_provider__["a" /* COOKIE_OPTIONS */], useValue: options },
                { provide: __WEBPACK_IMPORTED_MODULE_2__src_cookie_service__["a" /* CookieService */], useFactory: __WEBPACK_IMPORTED_MODULE_3__src_cookie_factory__["a" /* cookieServiceFactory */], deps: [__WEBPACK_IMPORTED_MODULE_1__src_cookie_options_provider__["b" /* CookieOptionsProvider */]] }
            ]
        };
    };
    CookieModule.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"], args: [{
                    providers: [__WEBPACK_IMPORTED_MODULE_1__src_cookie_options_provider__["b" /* CookieOptionsProvider */]]
                },] },
    ];
    /** @nocollapse */
    CookieModule.ctorParameters = function () { return []; };
    return CookieModule;
}());



/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = cookieServiceFactory;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__cookie_service__ = __webpack_require__(2);

function cookieServiceFactory(cookieOptionsProvider) {
    return new __WEBPACK_IMPORTED_MODULE_0__cookie_service__["a" /* CookieService */](cookieOptionsProvider);
}


/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_8__;

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var RestServiceConfig = (function () {
    function RestServiceConfig() {
    }
    return RestServiceConfig;
}());
exports.RestServiceConfig = RestServiceConfig;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var services_1 = __webpack_require__(4);
exports.RestClientService = services_1.RestClientService;
exports.RestServiceConfig = services_1.RestServiceConfig;
var rest_module_1 = __webpack_require__(20);
exports.RestClientModule = rest_module_1.RestClientModule;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var FileSaver = __webpack_require__(12);
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(5);
var ngx_cookie_1 = __webpack_require__(6);
var Subject_1 = __webpack_require__(17);
var operators_1 = __webpack_require__(8);
var operators_2 = __webpack_require__(8);
__webpack_require__(18);
__webpack_require__(19);
var rest_config_1 = __webpack_require__(9);
var RestClientService = (function () {
    /**
     * Service class constructor
     *
     * @param {HttpClient} http
     * @param {CookieService} cookies
     * @param {RestServiceConfig} config
     */
    function RestClientService(http, cookies, config) {
        this.http = http;
        this.cookies = cookies;
        /**
         * Handler used to stop all pending requests
         *
         * @type {Subject<boolean>}
         */
        this.cancelPending$ = new Subject_1.Subject();
        /**
         * Default requests header
         * @protected
         */
        this._baseHeader = {
            'accept': 'application/json',
            'Cache-Control': 'no-cache',
            'Pragma': 'no-cache',
            'Authorization': ''
        };
        /**
         * When true, the request header will include the authentication token
         *
         * @type boolean
         */
        this._secureRequest = false;
        /**
         * Rest API end point
         *
         * This value will be prepend to the request URL
         *
         * @type {string}
         * @private
         */
        this._endPoint = '';
        /**
         * API Authorization URI
         *
         * @type {string}
         * @private
         */
        this._authUri = '/authorize';
        /**
         * API Token Validation URI
         *
         * @type {string}
         * @private
         */
        this._validateTokenUri = '/validate-token';
        /**
         * Enable the Mock Data mode
         *
         * When Mock Data is enabled
         *
         * @type {boolean}
         * @private
         */
        this._mockData = false;
        /**
         * Name where the authorization token will be save
         *
         * @type {string}
         * @private
         */
        this._tokenName = 'AuthToken';
        /**
         * Name where the authorization token will be save
         *
         * @type {string}
         * @private
         */
        this._tokenStorage = 'cookie';
        /**
         * Set the expiration DateTime in minutes
         *
         * The value in minutes will be add to Datetime when the cookie is set
         *
         * @type {number}
         * @private
         */
        this._cookieExpires = 1440 * 7;
        /**
         * When true, cookies operation will be allow only when HTTPS is use
         *
         * @type {boolean}
         * @private
         */
        this._secureCookie = false;
        /**
         * Holds a list of files to be upload on request
         * @type {Array}
         */
        this._withFiles = false;
        if (config) {
            if (config.endPoint) {
                this._endPoint = config.endPoint;
            }
            if (config.tokenName) {
                this._tokenName = config.tokenName;
            }
            if (config.tokenStorage) {
                this._tokenName = config.tokenStorage;
            }
            if (config.secureCookie) {
                this._secureCookie = config.secureCookie;
            }
            if (config.mockData) {
                this._mockData = config.mockData;
            }
            if (config.cookieExpires) {
                this._cookieExpires = config.cookieExpires;
            }
        }
    }
    /**
     * Get the API Token from cookies
     *
     * @returns {string}
     */
    RestClientService.prototype.getToken = function () {
        var token = '';
        switch (this._tokenStorage) {
            case 'cookie':
                token = this.cookies.get(this._tokenName);
                break;
            case 'localStorage':
                token = JSON.parse(localStorage.getItem(this._tokenName));
                break;
            default:
                throw new Error('Invalid Token Storage method');
        }
        return !token || typeof token === 'undefined' ? '' : token;
    };
    /**
     * Request an Authorization token
     *
     * The default authorization URI is '[API_END_POINT]/authorize'
     *
     * @param uname
     * @param pws
     * @returns {Observable<any>}
     */
    RestClientService.prototype.authorize = function (uname, pws) {
        var _this = this;
        return this.request('post', this._authUri, { username: uname, password: pws })
            .do(function (data) {
            _this.setToken(data.token);
        });
    };
    /**
     * Validate the Authentication token against the API
     *
     * @returns {Observable<any>}
     */
    RestClientService.prototype.validateToken = function () {
        return this.request('post', this._validateTokenUri);
    };
    /**
     * Remove the Authentication token cookie
     */
    RestClientService.prototype.revoke = function () {
        switch (this._tokenStorage) {
            case 'cookie':
                this.cookies.removeAll();
                break;
            case 'localStorage':
                localStorage.removeItem(this._tokenName);
                break;
            default:
                throw new Error('Invalid Token Storage method');
        }
    };
    /**
     * Check if the client is already Authenticate
     *
     * @returns {boolean}
     */
    RestClientService.prototype.isAuthorized = function () {
        return this.getToken() !== '';
    };
    /**
     * Cancel all pending requests
     */
    RestClientService.prototype.cancelPendingRequests = function () {
        this.cancelPending$.next(true);
    };
    /**
     * API request using GET method
     *
     * @param {string} url
     * @param {{}} data
     * @returns {Observable<any>}
     */
    RestClientService.prototype.get = function (url, data) {
        return this.request('get', url, data);
    };
    /**
     * API request using POST method
     *
     * @param {string} url
     * @param {{}} data
     * @param {string} responseType
     * @param {HttpOptions} httpOptions
     * @returns {Observable<any>}
     */
    RestClientService.prototype.post = function (url, data, responseType, httpOptions) {
        if (httpOptions === void 0) { httpOptions = {}; }
        return this.request('post', url, data, responseType, httpOptions);
    };
    /**
     * API request using PUT method
     *
     * @param {string} url
     * @param {{}} data
     * @param {string} responseType
     * @param {HttpOptions} httpOptions
     * @returns {Observable<any>}
     */
    RestClientService.prototype.put = function (url, data, responseType, httpOptions) {
        if (httpOptions === void 0) { httpOptions = {}; }
        return this.request('put', url, data, responseType, httpOptions);
    };
    /**
     * Set the upload file mode
     *
     * @returns {RestClientService}
     */
    RestClientService.prototype.withFiles = function () {
        this._withFiles = true;
        return this;
    };
    /**
     * API request using DELETE method
     *
     * @param {string} url
     * @param {{}} data
     * @param {string} responseType
     * @returns {Observable<any>}
     */
    RestClientService.prototype.delete = function (url, data, responseType) {
        return this.request('delete', url, data, responseType);
    };
    /**
     * Set the request mode to SECURED for the next request.
     *
     * Secured Mode force the next request to include the authentication token.
     * The token must be requested previously using the "authorize" method.
     *
     * @returns {RestClientService}
     */
    RestClientService.prototype.secured = function () {
        this._secureRequest = true;
        return this;
    };
    /**
     * Set the request mode to PUBLIC for the next request.
     *
     * Public is the default request mode and ensure that no authentication token
     * will be pass on the next request.
     *
     * @returns {RestClientService}
     */
    RestClientService.prototype.public = function () {
        this._secureRequest = false;
        return this;
    };
    /**
     * Request a file from endpoint and pass the authenticate token if required
     *
     * @param {string} url
     * @param {string} fileName
     * @param {string} mime
     * @param {{}} data
     * @returns {Observable<any>}
     */
    RestClientService.prototype.download = function (url, fileName, mime, data) {
        // const msDelay = Math.floor((Math.random() * 2000) + 1000);
        var header = JSON.parse(JSON.stringify(this._baseHeader));
        if (this._secureRequest) {
            var token = this.getToken();
            if (!token) {
                console.warn('Executing a secure request without TOKEN.');
            }
            else {
                header.Authorization = "Bearer " + token;
            }
            this._secureRequest = false;
        }
        var options = { responseType: ('blob'), params: data, headers: header };
        return this.http.request('get', this.buildUrl(url), options)
            .pipe(operators_1.takeUntil(this.cancelPending$))
            .map(function (res) {
            var blob = new Blob([res], { type: mime });
            FileSaver.saveAs(blob, fileName);
            return 'DOWNLOAD';
        });
    };
    /**
     * Get the expiration Datetime for cookies
     *
     * Add (cookieExpires) minutes to current date
     *
     * @returns {Date}
     */
    RestClientService.prototype.getCookieExpires = function () {
        var d = new Date();
        d.setMinutes(d.getMinutes() + this._cookieExpires);
        return d;
    };
    /**
     * Save the API Token cookie
     *
     * @param token
     */
    RestClientService.prototype.setToken = function (token) {
        switch (this._tokenStorage) {
            case 'cookie':
                this.cookies.put(this._tokenName, token, { secure: this._secureCookie, expires: this.getCookieExpires() });
                break;
            case 'localStorage':
                localStorage.setItem(this._tokenName, token);
                break;
            default:
                throw new Error('Invalid Token Storage method');
        }
    };
    /**
     * Build a valid URL concatenating the url parameter with the ApiEndPoint
     *
     * @param url
     * @returns {string}
     */
    RestClientService.prototype.buildUrl = function (url) {
        var nUrl = this._endPoint.replace(/\/$/, '') + "/" + url.replace(/^\//g, '');
        var match = nUrl.match(/\.([0-9a-z]+)(?:[\?#]|$)/i);
        if (this._mockData && match == null) {
            nUrl = nUrl + ".json";
        }
        return nUrl;
    };
    /**
     * Create a FormData object to be send as request payload data
     *
     * @param object
     * @param form
     * @param namespace
     * @returns {FormData}
     */
    RestClientService.prototype.createFormData = function (object, form, namespace) {
        var formData = form || new FormData();
        for (var property in object) {
            if (!object.hasOwnProperty(property) || !object[property]) {
                continue;
            }
            var formKey = namespace ? namespace + "[" + property + "]" : property;
            if (object[property] instanceof Date) {
                formData.append(formKey, object[property].toISOString());
            }
            else if (typeof object[property] === 'object' && !(object[property] instanceof File)) {
                this.createFormData(object[property], formData, formKey);
            }
            else if (object[property] instanceof FileList) {
                formData.append(property + "[]", object[property]);
            }
            else {
                formData.append(formKey, object[property]);
            }
        }
        return formData;
    };
    /**
     * Raw request method
     */
    RestClientService.prototype.request = function (method, url, data, responseType, httpOptions) {
        if (httpOptions === void 0) { httpOptions = {}; }
        var msDelay = Math.floor((Math.random() * 2000) + 1000);
        var header = JSON.parse(JSON.stringify(this._baseHeader));
        if (this._secureRequest) {
            var token = this.getToken();
            if (!token) {
                console.warn('Executing a secure request without TOKEN. '
                    + 'Authorization header will not be set!');
            }
            else {
                header.Authorization = "Bearer " + token;
            }
            this._secureRequest = false;
        }
        var rType = (responseType ? responseType : 'json');
        var options = {
            body: method.toLowerCase() === 'get'
                ? {}
                : (this._withFiles ? this.createFormData(data) : data),
            responseType: rType,
            params: method.toLowerCase() === 'get' ? data : {},
            headers: header
        };
        this._withFiles = false;
        return this.http
            .request(this._mockData ? 'get' : method, this.buildUrl(url), __assign({}, options, httpOptions))
            .pipe(operators_1.takeUntil(this.cancelPending$))
            .pipe(operators_2.delay(this._mockData ? msDelay : 0));
    };
    RestClientService = __decorate([
        core_1.Injectable(),
        __param(2, core_1.Optional()),
        __metadata("design:paramtypes", [http_1.HttpClient,
            ngx_cookie_1.CookieService,
            rest_config_1.RestServiceConfig])
    ], RestClientService);
    return RestClientService;
}());
exports.RestClientService = RestClientService;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;/* FileSaver.js
 * A saveAs() FileSaver implementation.
 * 1.3.2
 * 2016-06-16 18:25:19
 *
 * By Eli Grey, http://eligrey.com
 * License: MIT
 *   See https://github.com/eligrey/FileSaver.js/blob/master/LICENSE.md
 */

/*global self */
/*jslint bitwise: true, indent: 4, laxbreak: true, laxcomma: true, smarttabs: true, plusplus: true */

/*! @source http://purl.eligrey.com/github/FileSaver.js/blob/master/FileSaver.js */

var saveAs = saveAs || (function(view) {
	"use strict";
	// IE <10 is explicitly unsupported
	if (typeof view === "undefined" || typeof navigator !== "undefined" && /MSIE [1-9]\./.test(navigator.userAgent)) {
		return;
	}
	var
		  doc = view.document
		  // only get URL when necessary in case Blob.js hasn't overridden it yet
		, get_URL = function() {
			return view.URL || view.webkitURL || view;
		}
		, save_link = doc.createElementNS("http://www.w3.org/1999/xhtml", "a")
		, can_use_save_link = "download" in save_link
		, click = function(node) {
			var event = new MouseEvent("click");
			node.dispatchEvent(event);
		}
		, is_safari = /constructor/i.test(view.HTMLElement) || view.safari
		, is_chrome_ios =/CriOS\/[\d]+/.test(navigator.userAgent)
		, throw_outside = function(ex) {
			(view.setImmediate || view.setTimeout)(function() {
				throw ex;
			}, 0);
		}
		, force_saveable_type = "application/octet-stream"
		// the Blob API is fundamentally broken as there is no "downloadfinished" event to subscribe to
		, arbitrary_revoke_timeout = 1000 * 40 // in ms
		, revoke = function(file) {
			var revoker = function() {
				if (typeof file === "string") { // file is an object URL
					get_URL().revokeObjectURL(file);
				} else { // file is a File
					file.remove();
				}
			};
			setTimeout(revoker, arbitrary_revoke_timeout);
		}
		, dispatch = function(filesaver, event_types, event) {
			event_types = [].concat(event_types);
			var i = event_types.length;
			while (i--) {
				var listener = filesaver["on" + event_types[i]];
				if (typeof listener === "function") {
					try {
						listener.call(filesaver, event || filesaver);
					} catch (ex) {
						throw_outside(ex);
					}
				}
			}
		}
		, auto_bom = function(blob) {
			// prepend BOM for UTF-8 XML and text/* types (including HTML)
			// note: your browser will automatically convert UTF-16 U+FEFF to EF BB BF
			if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(blob.type)) {
				return new Blob([String.fromCharCode(0xFEFF), blob], {type: blob.type});
			}
			return blob;
		}
		, FileSaver = function(blob, name, no_auto_bom) {
			if (!no_auto_bom) {
				blob = auto_bom(blob);
			}
			// First try a.download, then web filesystem, then object URLs
			var
				  filesaver = this
				, type = blob.type
				, force = type === force_saveable_type
				, object_url
				, dispatch_all = function() {
					dispatch(filesaver, "writestart progress write writeend".split(" "));
				}
				// on any filesys errors revert to saving with object URLs
				, fs_error = function() {
					if ((is_chrome_ios || (force && is_safari)) && view.FileReader) {
						// Safari doesn't allow downloading of blob urls
						var reader = new FileReader();
						reader.onloadend = function() {
							var url = is_chrome_ios ? reader.result : reader.result.replace(/^data:[^;]*;/, 'data:attachment/file;');
							var popup = view.open(url, '_blank');
							if(!popup) view.location.href = url;
							url=undefined; // release reference before dispatching
							filesaver.readyState = filesaver.DONE;
							dispatch_all();
						};
						reader.readAsDataURL(blob);
						filesaver.readyState = filesaver.INIT;
						return;
					}
					// don't create more object URLs than needed
					if (!object_url) {
						object_url = get_URL().createObjectURL(blob);
					}
					if (force) {
						view.location.href = object_url;
					} else {
						var opened = view.open(object_url, "_blank");
						if (!opened) {
							// Apple does not allow window.open, see https://developer.apple.com/library/safari/documentation/Tools/Conceptual/SafariExtensionGuide/WorkingwithWindowsandTabs/WorkingwithWindowsandTabs.html
							view.location.href = object_url;
						}
					}
					filesaver.readyState = filesaver.DONE;
					dispatch_all();
					revoke(object_url);
				}
			;
			filesaver.readyState = filesaver.INIT;

			if (can_use_save_link) {
				object_url = get_URL().createObjectURL(blob);
				setTimeout(function() {
					save_link.href = object_url;
					save_link.download = name;
					click(save_link);
					dispatch_all();
					revoke(object_url);
					filesaver.readyState = filesaver.DONE;
				});
				return;
			}

			fs_error();
		}
		, FS_proto = FileSaver.prototype
		, saveAs = function(blob, name, no_auto_bom) {
			return new FileSaver(blob, name || blob.name || "download", no_auto_bom);
		}
	;
	// IE 10+ (native saveAs)
	if (typeof navigator !== "undefined" && navigator.msSaveOrOpenBlob) {
		return function(blob, name, no_auto_bom) {
			name = name || blob.name || "download";

			if (!no_auto_bom) {
				blob = auto_bom(blob);
			}
			return navigator.msSaveOrOpenBlob(blob, name);
		};
	}

	FS_proto.abort = function(){};
	FS_proto.readyState = FS_proto.INIT = 0;
	FS_proto.WRITING = 1;
	FS_proto.DONE = 2;

	FS_proto.error =
	FS_proto.onwritestart =
	FS_proto.onprogress =
	FS_proto.onwrite =
	FS_proto.onabort =
	FS_proto.onerror =
	FS_proto.onwriteend =
		null;

	return saveAs;
}(
	   typeof self !== "undefined" && self
	|| typeof window !== "undefined" && window
	|| this.content
));
// `self` is undefined in Firefox for Android content script context
// while `this` is nsIContentFrameMessageManager
// with an attribute `content` that corresponds to the window

if (typeof module !== "undefined" && module.exports) {
  module.exports.saveAs = saveAs;
} else if (("function" !== "undefined" && __webpack_require__(13) !== null) && (__webpack_require__(14) !== null)) {
  !(__WEBPACK_AMD_DEFINE_RESULT__ = (function() {
    return saveAs;
  }).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
}


/***/ }),
/* 13 */
/***/ (function(module, exports) {

module.exports = function() {
	throw new Error("define cannot be used indirect");
};


/***/ }),
/* 14 */
/***/ (function(module, exports) {

/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {/* globals __webpack_amd_options__ */
module.exports = __webpack_amd_options__;

/* WEBPACK VAR INJECTION */}.call(exports, {}))

/***/ }),
/* 15 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_15__;

/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CookieBackendService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__angular_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__cookie_service__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cookie_options_provider__ = __webpack_require__(1);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var CookieBackendService = (function (_super) {
    __extends(CookieBackendService, _super);
    function CookieBackendService(request, response, _optionsProvider) {
        var _this = _super.call(this, _optionsProvider) || this;
        _this.request = request;
        _this.response = response;
        return _this;
    }
    Object.defineProperty(CookieBackendService.prototype, "cookieString", {
        get: function () {
            return this.request.headers.cookie || '';
        },
        set: function (val) {
            this.request.headers.cookie = val;
            this.response.headers.cookie = val;
        },
        enumerable: true,
        configurable: true
    });
    CookieBackendService.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    /** @nocollapse */
    CookieBackendService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"], args: ['REQUEST',] },] },
        { type: undefined, decorators: [{ type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Inject"], args: ['RESPONSE',] },] },
        { type: __WEBPACK_IMPORTED_MODULE_2__cookie_options_provider__["b" /* CookieOptionsProvider */], },
    ]; };
    return CookieBackendService;
}(__WEBPACK_IMPORTED_MODULE_1__cookie_service__["a" /* CookieService */]));



/***/ }),
/* 17 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_17__;

/***/ }),
/* 18 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_18__;

/***/ }),
/* 19 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_19__;

/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(5);
var ngx_cookie_1 = __webpack_require__(6);
var services_1 = __webpack_require__(4);
var RestClientModule = (function () {
    function RestClientModule() {
    }
    RestClientModule_1 = RestClientModule;
    RestClientModule.forRoot = function (config) {
        return {
            ngModule: RestClientModule_1,
            providers: [
                { provide: services_1.RestServiceConfig, useValue: config }
            ]
        };
    };
    RestClientModule = RestClientModule_1 = __decorate([
        core_1.NgModule({
            imports: [
                http_1.HttpClientModule,
                ngx_cookie_1.CookieModule.forRoot()
            ],
            providers: [
                http_1.HttpClient,
                ngx_cookie_1.CookieService,
                services_1.RestClientService
            ]
        })
    ], RestClientModule);
    return RestClientModule;
    var RestClientModule_1;
}());
exports.RestClientModule = RestClientModule;


/***/ })
/******/ ]);
});
//# sourceMappingURL=index.umd.js.map