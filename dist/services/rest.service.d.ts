import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { CookieService } from 'ngx-cookie';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { RestServiceConfig } from './rest.config';
export declare type HttpObserve = 'body' | 'events' | 'response';
export interface HttpOptions {
    body?: any;
    headers?: HttpHeaders | {
        [header: string]: string | string[];
    };
    params?: HttpParams | {
        [param: string]: string | string[];
    };
    observe?: HttpObserve;
    reportProgress?: boolean;
    responseType?: 'arraybuffer' | 'blob' | 'json' | 'text';
    withCredentials?: boolean;
}
export declare class RestClientService {
    private http;
    private cookies;
    /**
     * Handler used to stop all pending requests
     *
     * @type {Subject<boolean>}
     */
    protected cancelPending$: Subject<boolean>;
    /**
     * Default requests header
     * @protected
     */
    protected _baseHeader: {
        'accept': string;
        'Cache-Control': string;
        'Pragma': string;
        'Authorization': string;
    };
    /**
     * When true, the request header will include the authentication token
     *
     * @type boolean
     */
    protected _secureRequest: boolean;
    /**
     * Rest API end point
     *
     * This value will be prepend to the request URL
     *
     * @type {string}
     * @private
     */
    protected _endPoint: string;
    /**
     * API Authorization URI
     *
     * @type {string}
     * @private
     */
    protected _authUri: string;
    /**
     * API Token Validation URI
     *
     * @type {string}
     * @private
     */
    protected _validateTokenUri: string;
    /**
     * Enable the Mock Data mode
     *
     * When Mock Data is enabled
     *
     * @type {boolean}
     * @private
     */
    protected _mockData: boolean;
    /**
     * Name where the authorization token will be save
     *
     * @type {string}
     * @private
     */
    protected _tokenName: string;
    /**
     * Name where the authorization token will be save
     *
     * @type {string}
     * @private
     */
    protected _tokenStorage: string;
    /**
     * Set the expiration DateTime in minutes
     *
     * The value in minutes will be add to Datetime when the cookie is set
     *
     * @type {number}
     * @private
     */
    protected _cookieExpires: number;
    /**
     * When true, cookies operation will be allow only when HTTPS is use
     *
     * @type {boolean}
     * @private
     */
    protected _secureCookie: boolean;
    /**
     * Holds a list of files to be upload on request
     * @type {Array}
     */
    protected _withFiles: boolean;
    /**
     * Service class constructor
     *
     * @param {HttpClient} http
     * @param {CookieService} cookies
     * @param {RestServiceConfig} config
     */
    constructor(http: HttpClient, cookies: CookieService, config: RestServiceConfig);
    /**
     * Get the API Token from cookies
     *
     * @returns {string}
     */
    getToken(): string;
    /**
     * Request an Authorization token
     *
     * The default authorization URI is '[API_END_POINT]/authorize'
     *
     * @param uname
     * @param pws
     * @returns {Observable<any>}
     */
    authorize(uname: string, pws: string): Observable<any>;
    /**
     * Validate the Authentication token against the API
     *
     * @returns {Observable<any>}
     */
    validateToken(): Observable<any>;
    /**
     * Remove the Authentication token cookie
     */
    revoke(): void;
    /**
     * Check if the client is already Authenticate
     *
     * @returns {boolean}
     */
    isAuthorized(): boolean;
    /**
     * Cancel all pending requests
     */
    cancelPendingRequests(): void;
    /**
     * API request using GET method
     *
     * @param {string} url
     * @param {{}} data
     * @returns {Observable<any>}
     */
    get(url: string, data?: {}): Observable<any>;
    /**
     * API request using POST method
     *
     * @param {string} url
     * @param {{}} data
     * @param {string} responseType
     * @param {HttpOptions} httpOptions
     * @returns {Observable<any>}
     */
    post(url: string, data?: {}, responseType?: string, httpOptions?: HttpOptions): Observable<any>;
    /**
     * API request using PUT method
     *
     * @param {string} url
     * @param {{}} data
     * @param {string} responseType
     * @param {HttpOptions} httpOptions
     * @returns {Observable<any>}
     */
    put(url: string, data?: {}, responseType?: string, httpOptions?: HttpOptions): Observable<any>;
    /**
     * Set the upload file mode
     *
     * @returns {RestClientService}
     */
    withFiles(): RestClientService;
    /**
     * API request using DELETE method
     *
     * @param {string} url
     * @param {{}} data
     * @param {string} responseType
     * @returns {Observable<any>}
     */
    delete(url: string, data?: {}, responseType?: string): Observable<any>;
    /**
     * Set the request mode to SECURED for the next request.
     *
     * Secured Mode force the next request to include the authentication token.
     * The token must be requested previously using the "authorize" method.
     *
     * @returns {RestClientService}
     */
    secured(): this;
    /**
     * Set the request mode to PUBLIC for the next request.
     *
     * Public is the default request mode and ensure that no authentication token
     * will be pass on the next request.
     *
     * @returns {RestClientService}
     */
    public(): this;
    /**
     * Request a file from endpoint and pass the authenticate token if required
     *
     * @param {string} url
     * @param {string} fileName
     * @param {string} mime
     * @param {{}} data
     * @returns {Observable<any>}
     */
    download(url: string, fileName: string, mime: string, data?: {}): Observable<any>;
    /**
     * Get the expiration Datetime for cookies
     *
     * Add (cookieExpires) minutes to current date
     *
     * @returns {Date}
     */
    protected getCookieExpires(): Date;
    /**
     * Save the API Token cookie
     *
     * @param token
     */
    protected setToken(token: string): void;
    /**
     * Build a valid URL concatenating the url parameter with the ApiEndPoint
     *
     * @param url
     * @returns {string}
     */
    protected buildUrl(url: string): string;
    /**
     * Create a FormData object to be send as request payload data
     *
     * @param object
     * @param form
     * @param namespace
     * @returns {FormData}
     */
    protected createFormData(object: any, form?: FormData, namespace?: string): FormData;
    /**
     * Raw request method
     */
    protected request(method: string, url: string, data?: any, responseType?: string, httpOptions?: HttpOptions): Observable<any>;
}
