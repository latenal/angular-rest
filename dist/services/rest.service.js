var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
import * as FileSaver from 'file-saver';
import { Injectable, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { delay } from 'rxjs/operators';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import { RestServiceConfig } from './rest.config';
var RestClientService = (function () {
    /**
     * Service class constructor
     *
     * @param {HttpClient} http
     * @param {CookieService} cookies
     * @param {RestServiceConfig} config
     */
    function RestClientService(http, cookies, config) {
        this.http = http;
        this.cookies = cookies;
        /**
             * Handler used to stop all pending requests
             *
             * @type {Subject<boolean>}
             */
        this.cancelPending$ = new Subject();
        /**
             * Default requests header
             * @protected
             */
        this._baseHeader = {
            'accept': 'application/json',
            'Cache-Control': 'no-cache',
            'Pragma': 'no-cache',
            'Authorization': ''
        };
        /**
             * When true, the request header will include the authentication token
             *
             * @type boolean
             */
        this._secureRequest = false;
        /**
             * Rest API end point
             *
             * This value will be prepend to the request URL
             *
             * @type {string}
             * @private
             */
        this._endPoint = '';
        /**
             * API Authorization URI
             *
             * @type {string}
             * @private
             */
        this._authUri = '/authorize';
        /**
             * API Token Validation URI
             *
             * @type {string}
             * @private
             */
        this._validateTokenUri = '/validate-token';
        /**
             * Enable the Mock Data mode
             *
             * When Mock Data is enabled
             *
             * @type {boolean}
             * @private
             */
        this._mockData = false;
        /**
             * Name where the authorization token will be save
             *
             * @type {string}
             * @private
             */
        this._tokenName = 'AuthToken';
        /**
             * Name where the authorization token will be save
             *
             * @type {string}
             * @private
             */
        this._tokenStorage = 'cookie';
        /**
             * Set the expiration DateTime in minutes
             *
             * The value in minutes will be add to Datetime when the cookie is set
             *
             * @type {number}
             * @private
             */
        this._cookieExpires = 1440 * 7;
        /**
             * When true, cookies operation will be allow only when HTTPS is use
             *
             * @type {boolean}
             * @private
             */
        this._secureCookie = false;
        /**
             * Holds a list of files to be upload on request
             * @type {Array}
             */
        this._withFiles = false;
        if (config) {
            if (config.endPoint) {
                this._endPoint = config.endPoint;
            }
            if (config.tokenName) {
                this._tokenName = config.tokenName;
            }
            if (config.tokenStorage) {
                this._tokenName = config.tokenStorage;
            }
            if (config.secureCookie) {
                this._secureCookie = config.secureCookie;
            }
            if (config.mockData) {
                this._mockData = config.mockData;
            }
            if (config.cookieExpires) {
                this._cookieExpires = config.cookieExpires;
            }
        }
    }
    /**
     * Get the API Token from cookies
     *
     * @returns {string}
     */
    /**
         * Get the API Token from cookies
         *
         * @returns {string}
         */
    RestClientService.prototype.getToken = /**
         * Get the API Token from cookies
         *
         * @returns {string}
         */
    function () {
        var token = '';
        switch (this._tokenStorage) {
            case 'cookie':
                token = this.cookies.get(this._tokenName);
                break;
            case 'localStorage':
                token = JSON.parse(localStorage.getItem(this._tokenName));
                break;
            default:
                throw new Error('Invalid Token Storage method');
        }
        return !token || typeof token === 'undefined' ? '' : token;
    };
    /**
     * Request an Authorization token
     *
     * The default authorization URI is '[API_END_POINT]/authorize'
     *
     * @param uname
     * @param pws
     * @returns {Observable<any>}
     */
    /**
         * Request an Authorization token
         *
         * The default authorization URI is '[API_END_POINT]/authorize'
         *
         * @param uname
         * @param pws
         * @returns {Observable<any>}
         */
    RestClientService.prototype.authorize = /**
         * Request an Authorization token
         *
         * The default authorization URI is '[API_END_POINT]/authorize'
         *
         * @param uname
         * @param pws
         * @returns {Observable<any>}
         */
    function (uname, pws) {
        var _this = this;
        return this.request('post', this._authUri, { username: uname, password: pws })
            .do(function (data) {
            _this.setToken(data.token);
        });
    };
    /**
     * Validate the Authentication token against the API
     *
     * @returns {Observable<any>}
     */
    /**
         * Validate the Authentication token against the API
         *
         * @returns {Observable<any>}
         */
    RestClientService.prototype.validateToken = /**
         * Validate the Authentication token against the API
         *
         * @returns {Observable<any>}
         */
    function () {
        return this.request('post', this._validateTokenUri);
    };
    /**
     * Remove the Authentication token cookie
     */
    /**
         * Remove the Authentication token cookie
         */
    RestClientService.prototype.revoke = /**
         * Remove the Authentication token cookie
         */
    function () {
        switch (this._tokenStorage) {
            case 'cookie':
                this.cookies.removeAll();
                break;
            case 'localStorage':
                localStorage.removeItem(this._tokenName);
                break;
            default:
                throw new Error('Invalid Token Storage method');
        }
    };
    /**
     * Check if the client is already Authenticate
     *
     * @returns {boolean}
     */
    /**
         * Check if the client is already Authenticate
         *
         * @returns {boolean}
         */
    RestClientService.prototype.isAuthorized = /**
         * Check if the client is already Authenticate
         *
         * @returns {boolean}
         */
    function () {
        return this.getToken() !== '';
    };
    /**
     * Cancel all pending requests
     */
    /**
         * Cancel all pending requests
         */
    RestClientService.prototype.cancelPendingRequests = /**
         * Cancel all pending requests
         */
    function () {
        this.cancelPending$.next(true);
    };
    /**
     * API request using GET method
     *
     * @param {string} url
     * @param {{}} data
     * @returns {Observable<any>}
     */
    /**
         * API request using GET method
         *
         * @param {string} url
         * @param {{}} data
         * @returns {Observable<any>}
         */
    RestClientService.prototype.get = /**
         * API request using GET method
         *
         * @param {string} url
         * @param {{}} data
         * @returns {Observable<any>}
         */
    function (url, data) {
        return this.request('get', url, data);
    };
    /**
     * API request using POST method
     *
     * @param {string} url
     * @param {{}} data
     * @param {string} responseType
     * @param {HttpOptions} httpOptions
     * @returns {Observable<any>}
     */
    /**
         * API request using POST method
         *
         * @param {string} url
         * @param {{}} data
         * @param {string} responseType
         * @param {HttpOptions} httpOptions
         * @returns {Observable<any>}
         */
    RestClientService.prototype.post = /**
         * API request using POST method
         *
         * @param {string} url
         * @param {{}} data
         * @param {string} responseType
         * @param {HttpOptions} httpOptions
         * @returns {Observable<any>}
         */
    function (url, data, responseType, httpOptions) {
        if (httpOptions === void 0) { httpOptions = {}; }
        return this.request('post', url, data, responseType, httpOptions);
    };
    /**
     * API request using PUT method
     *
     * @param {string} url
     * @param {{}} data
     * @param {string} responseType
     * @param {HttpOptions} httpOptions
     * @returns {Observable<any>}
     */
    /**
         * API request using PUT method
         *
         * @param {string} url
         * @param {{}} data
         * @param {string} responseType
         * @param {HttpOptions} httpOptions
         * @returns {Observable<any>}
         */
    RestClientService.prototype.put = /**
         * API request using PUT method
         *
         * @param {string} url
         * @param {{}} data
         * @param {string} responseType
         * @param {HttpOptions} httpOptions
         * @returns {Observable<any>}
         */
    function (url, data, responseType, httpOptions) {
        if (httpOptions === void 0) { httpOptions = {}; }
        return this.request('put', url, data, responseType, httpOptions);
    };
    /**
     * Set the upload file mode
     *
     * @returns {RestClientService}
     */
    /**
         * Set the upload file mode
         *
         * @returns {RestClientService}
         */
    RestClientService.prototype.withFiles = /**
         * Set the upload file mode
         *
         * @returns {RestClientService}
         */
    function () {
        this._withFiles = true;
        return this;
    };
    /**
     * API request using DELETE method
     *
     * @param {string} url
     * @param {{}} data
     * @param {string} responseType
     * @returns {Observable<any>}
     */
    /**
         * API request using DELETE method
         *
         * @param {string} url
         * @param {{}} data
         * @param {string} responseType
         * @returns {Observable<any>}
         */
    RestClientService.prototype.delete = /**
         * API request using DELETE method
         *
         * @param {string} url
         * @param {{}} data
         * @param {string} responseType
         * @returns {Observable<any>}
         */
    function (url, data, responseType) {
        return this.request('delete', url, data, responseType);
    };
    /**
     * Set the request mode to SECURED for the next request.
     *
     * Secured Mode force the next request to include the authentication token.
     * The token must be requested previously using the "authorize" method.
     *
     * @returns {RestClientService}
     */
    /**
         * Set the request mode to SECURED for the next request.
         *
         * Secured Mode force the next request to include the authentication token.
         * The token must be requested previously using the "authorize" method.
         *
         * @returns {RestClientService}
         */
    RestClientService.prototype.secured = /**
         * Set the request mode to SECURED for the next request.
         *
         * Secured Mode force the next request to include the authentication token.
         * The token must be requested previously using the "authorize" method.
         *
         * @returns {RestClientService}
         */
    function () {
        this._secureRequest = true;
        return this;
    };
    /**
     * Set the request mode to PUBLIC for the next request.
     *
     * Public is the default request mode and ensure that no authentication token
     * will be pass on the next request.
     *
     * @returns {RestClientService}
     */
    /**
         * Set the request mode to PUBLIC for the next request.
         *
         * Public is the default request mode and ensure that no authentication token
         * will be pass on the next request.
         *
         * @returns {RestClientService}
         */
    RestClientService.prototype.public = /**
         * Set the request mode to PUBLIC for the next request.
         *
         * Public is the default request mode and ensure that no authentication token
         * will be pass on the next request.
         *
         * @returns {RestClientService}
         */
    function () {
        this._secureRequest = false;
        return this;
    };
    /**
     * Request a file from endpoint and pass the authenticate token if required
     *
     * @param {string} url
     * @param {string} fileName
     * @param {string} mime
     * @param {{}} data
     * @returns {Observable<any>}
     */
    /**
         * Request a file from endpoint and pass the authenticate token if required
         *
         * @param {string} url
         * @param {string} fileName
         * @param {string} mime
         * @param {{}} data
         * @returns {Observable<any>}
         */
    RestClientService.prototype.download = /**
         * Request a file from endpoint and pass the authenticate token if required
         *
         * @param {string} url
         * @param {string} fileName
         * @param {string} mime
         * @param {{}} data
         * @returns {Observable<any>}
         */
    function (url, fileName, mime, data) {
        // const msDelay = Math.floor((Math.random() * 2000) + 1000);
        var header = JSON.parse(JSON.stringify(this._baseHeader));
        if (this._secureRequest) {
            var token = this.getToken();
            if (!token) {
                console.warn('Executing a secure request without TOKEN.');
            }
            else {
                header.Authorization = "Bearer " + token;
            }
            this._secureRequest = false;
        }
        var options = { responseType: ('blob'), params: data, headers: header };
        return this.http.request('get', this.buildUrl(url), options)
            .pipe(takeUntil(this.cancelPending$))
            .map(function (res) {
            var blob = new Blob([res], { type: mime });
            FileSaver.saveAs(blob, fileName);
            return 'DOWNLOAD';
        });
    };
    /**
     * Get the expiration Datetime for cookies
     *
     * Add (cookieExpires) minutes to current date
     *
     * @returns {Date}
     */
    /**
         * Get the expiration Datetime for cookies
         *
         * Add (cookieExpires) minutes to current date
         *
         * @returns {Date}
         */
    RestClientService.prototype.getCookieExpires = /**
         * Get the expiration Datetime for cookies
         *
         * Add (cookieExpires) minutes to current date
         *
         * @returns {Date}
         */
    function () {
        var d = new Date();
        d.setMinutes(d.getMinutes() + this._cookieExpires);
        return d;
    };
    /**
     * Save the API Token cookie
     *
     * @param token
     */
    /**
         * Save the API Token cookie
         *
         * @param token
         */
    RestClientService.prototype.setToken = /**
         * Save the API Token cookie
         *
         * @param token
         */
    function (token) {
        switch (this._tokenStorage) {
            case 'cookie':
                this.cookies.put(this._tokenName, token, { secure: this._secureCookie, expires: this.getCookieExpires() });
                break;
            case 'localStorage':
                localStorage.setItem(this._tokenName, token);
                break;
            default:
                throw new Error('Invalid Token Storage method');
        }
    };
    /**
     * Build a valid URL concatenating the url parameter with the ApiEndPoint
     *
     * @param url
     * @returns {string}
     */
    /**
         * Build a valid URL concatenating the url parameter with the ApiEndPoint
         *
         * @param url
         * @returns {string}
         */
    RestClientService.prototype.buildUrl = /**
         * Build a valid URL concatenating the url parameter with the ApiEndPoint
         *
         * @param url
         * @returns {string}
         */
    function (url) {
        var nUrl = this._endPoint.replace(/\/$/, '') + "/" + url.replace(/^\//g, '');
        var match = nUrl.match(/\.([0-9a-z]+)(?:[\?#]|$)/i);
        if (this._mockData && match == null) {
            nUrl = nUrl + ".json";
        }
        return nUrl;
    };
    /**
     * Create a FormData object to be send as request payload data
     *
     * @param object
     * @param form
     * @param namespace
     * @returns {FormData}
     */
    /**
         * Create a FormData object to be send as request payload data
         *
         * @param object
         * @param form
         * @param namespace
         * @returns {FormData}
         */
    RestClientService.prototype.createFormData = /**
         * Create a FormData object to be send as request payload data
         *
         * @param object
         * @param form
         * @param namespace
         * @returns {FormData}
         */
    function (object, form, namespace) {
        var formData = form || new FormData();
        for (var property in object) {
            if (!object.hasOwnProperty(property) || !object[property]) {
                continue;
            }
            var formKey = namespace ? namespace + "[" + property + "]" : property;
            if (object[property] instanceof Date) {
                formData.append(formKey, object[property].toISOString());
            }
            else if (typeof object[property] === 'object' && !(object[property] instanceof File)) {
                this.createFormData(object[property], formData, formKey);
            }
            else if (object[property] instanceof FileList) {
                formData.append(property + "[]", object[property]);
            }
            else {
                formData.append(formKey, object[property]);
            }
        }
        return formData;
    };
    /**
     * Raw request method
     */
    /**
         * Raw request method
         */
    RestClientService.prototype.request = /**
         * Raw request method
         */
    function (method, url, data, responseType, httpOptions) {
        if (httpOptions === void 0) { httpOptions = {}; }
        var msDelay = Math.floor((Math.random() * 2000) + 1000);
        var header = JSON.parse(JSON.stringify(this._baseHeader));
        if (this._secureRequest) {
            var token = this.getToken();
            if (!token) {
                console.warn('Executing a secure request without TOKEN. '
                    + 'Authorization header will not be set!');
            }
            else {
                header.Authorization = "Bearer " + token;
            }
            this._secureRequest = false;
        }
        var rType = (responseType ? responseType : 'json');
        var options = {
            body: method.toLowerCase() === 'get'
                ? {}
                : (this._withFiles ? this.createFormData(data) : data),
            responseType: rType,
            params: method.toLowerCase() === 'get' ? data : {},
            headers: header
        };
        this._withFiles = false;
        return this.http
            .request(this._mockData ? 'get' : method, this.buildUrl(url), __assign({}, options, httpOptions))
            .pipe(takeUntil(this.cancelPending$))
            .pipe(delay(this._mockData ? msDelay : 0));
    };
    RestClientService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    RestClientService.ctorParameters = function () { return [
        { type: HttpClient, },
        { type: CookieService, },
        { type: RestServiceConfig, decorators: [{ type: Optional },] },
    ]; };
    return RestClientService;
}());
export { RestClientService };
//# sourceMappingURL=rest.service.js.map