import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CookieModule, CookieService } from 'ngx-cookie';
import { RestServiceConfig, RestClientService } from './services';
var RestClientModule = (function () {
    function RestClientModule() {
    }
    RestClientModule.forRoot = function (config) {
        return {
            ngModule: RestClientModule,
            providers: [
                { provide: RestServiceConfig, useValue: config }
            ]
        };
    };
    RestClientModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        HttpClientModule,
                        CookieModule.forRoot()
                    ],
                    providers: [
                        HttpClient,
                        CookieService,
                        RestClientService
                    ]
                },] },
    ];
    /** @nocollapse */
    RestClientModule.ctorParameters = function () { return []; };
    return RestClientModule;
}());
export { RestClientModule };
//# sourceMappingURL=rest.module.js.map