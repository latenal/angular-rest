import { ModuleWithProviders, NgModule} from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CookieModule, CookieService } from 'ngx-cookie';

import { RestServiceConfig, RestClientService } from './services';

@NgModule({
    imports: [
        HttpClientModule,
        CookieModule.forRoot()
    ],
    providers: [
        HttpClient,
        CookieService,
        RestClientService
    ]
})
export class RestClientModule {
    public static forRoot( config?: RestServiceConfig ): ModuleWithProviders {
        return {
            ngModule: RestClientModule,
            providers: [
                {provide: RestServiceConfig, useValue: config }
            ]
        };
    }
}
