# Angular Rest API Client Service #

### Dependencies ###
* [ngx-cookie](https://github.com/salemdar/ngx-cookie): Implementation of Angular 1.x $cookies service to Angular 2+
* [file-saver](https://github.com/eligrey/FileSaver.js): An HTML5 saveAs() FileSaver implementation


### Installation ###
``` bash
npm install --save file-saver ngx-cookie@2.0.1 
npm install --save git+ssh://git@gitlab.com/alus/angular-rest.git
```

### Setup and Service configuration parameters ###

Parameter     | Default value | Data Type     | Description
------------- | ------------- | ------------- | -------------
cookieTokenName | "AppToken" | string | The name of the cookie where the authentication token will be save
endPoint | "/" | string | The api end point URL
mockData | true | boolean | Set the Mock Data mode. If true, the extension ".json" will be append tho the end of each URL request and will be added a random delay to emulate response time
secureCookie | false | boolean | When enabled (true), HTTPS is required for secured API requests
cookieExpires | 1440*7 | number(minutes) | Set the number the minutes the cookies will be valid 
authUri | "/authorize" | string | URI to be used for authorization token request 
validationTokenUri | "/validate-token" | string | URI to be used for token validation request 

#### Setup Example ####

The configuration parameters must be past via .forRoot({ }) static method:

``` typescript
/**
 * Sample App main Module
 **/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RestClientModule } from 'angular-rest';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
      RestClientModule.forRoot({ endPoint: 'assets/mock-data/', mockData: true })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


/**
 * Sample App main component
 **/
import { Component } from '@angular/core';
import { RestClientService } from 'angular-rest';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(
      private http: RestClientService
  ){}

  public doRequest(){
    this.http.authorize('asdf@asdf.com','12345')
        .subscribe(
            (data)=>{ console.log(data); },
            (err)=>{ console.log(err); }
        )
  }

}
```