# Angular Rest API Client Service
## How To ##

#### File Upload ####
```typescript
import { Component, ElementRef, ViewChild } from '@angular/core';
import { RestClientService } from "angular-rest";

@Component({
    selector: 'upload',
    styleUrls: [ './upload.component.scss' ],
    template: `
		<h3>Upload File Sample</h3>
		<input type="file" #fileInput/>
        <button (click)="submit($event)">Upload!</button>	
	`
})
export class UploadComponent {

    @ViewChild( 'fileInput' ) files: ElementRef;

    constructor( private rest: RestClientService ) { }

    submit($event) {
        this.rest
            .secured()
            .withFiles()
            .post( 
                'images/upload/', { 
                    id: 1,
                    report_image_logo: this.files.nativeElement.files
            } )
            .subscribe( 
                ( data ) => { console.log('Success'); },
                ( data ) => { console.log('Error'); }
            );
    }

}
```