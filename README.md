# Angular Rest API Client Service #
Now with Angular 5 JIT + AOT Support

* [Installation and Setup](documentation/INSTALLATION.md)
* ["How To" with examples](documentation/HOW_TO.md)
* [Service Class Documentation](documentation/SERVICE.md)
